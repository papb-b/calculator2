package easy.tuto.easycalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;


public class MainActivity2 extends AppCompatActivity {

    TextView textView,textView2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);

        Intent in = getIntent();
        Bundle b = in.getExtras();

        String hasil = b.getString(MainActivity.HASIL);
        String operasi =b.getString(MainActivity.SOLUSI);

        textView.setText(operasi);
        textView2.setText(hasil);
    }
}